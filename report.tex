% Created 2017-11-19 Sun 12:20
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{fixltx2e}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{float}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{marvosym}
\usepackage{wasysym}
\usepackage{amssymb}
\usepackage{hyperref}
\tolerance=1000
\author{Vihari Piratla (173050039) Jagadeesha Kanihal (163059002)}
\date{\today}
\title{Assignment02 Report}
\hypersetup{
  pdfkeywords={},
  pdfsubject={},
  pdfcreator={Emacs 24.5.1 (Org mode 8.2.10)}}
\begin{document}

\maketitle
\tableofcontents


\section{Abstract}
\label{sec-1}
We compare several generative and discriminative classifiers for
document classification. Through experiments, we qualitatively analyse 
the implications of carefully modelling word distributions in a simple 
Naive Bayes method on document classification accuracy.

Dirichlet Compounded Multinomial (DCM), referred to as Polya
hereafter, is used to model word distributions. The Charles Elkan's
paper\footnote{\href{https://dl.acm.org/citation.cfm?id\%3D1102420}{Modeling word burstiness using the Dirichlet distribution}} details the learning of Polya distribution for document
classification task. Inference and conditional of the Polya distribution is
implemented using further details from \footnote{"Estimating a Dirichlet distribution" \url{https://tminka.github.io/papers/dirichlet/minka-dirichlet.pdf}}. Our implementation is a
python port of this Matlab toolbox\footnote{\url{https://github.com/tminka/fastfit/}}.

It is found through our experiments that Polya based on Naive Bayes model
(NBPolya) performed better than Multinomial based Naive Bayes model, although the improvements are
only slight. 
In the Conclusion section \texttt{sec:conclusion}, we further analyse the question: what is the biggest disadvantage of
Naive Bayes Model?

\section{Folder Contents}
\label{sec-2}
\begin{itemize}
\item \emph{polya.py} is the DCM inference and likelihood estimation routine
\item \emph{polynb.py} is the Naive Bayes Implementation using the Polya model
\item \emph{document$_{\text{classification}}$$_{\text{20newsgroups}}$.py} runs several document
classification models on newsgroup dataset.
\end{itemize}

\section{Experiments}
\label{sec-3}
\subsection{Datasets}
\label{sec-3-1}
The 20 newsgroup dataset is used as a test-bed for all the
experiments.
It contains 18,941 documents, with around 60K unique words. 
One of the versions of the dataset comes with a train and test split (of 60\% and 40\% sizes), test split are the
messages that appear at later time step.

\subsection{Feature Extraction}
\label{sec-3-2}
The documents are pre-processed to remove all the known stop words
in English. 
Also removed are the meta-data content in the text which makes
the (document classification) task easy: such as headers, footers and quotes. Refer to \emph{remove}
flag of this python method\footnote{\url{http://scikit-learn.org/stable/modules/generated/sklearn.datasets.fetch_20newsgroups.html#sklearn.datasets.fetch_20newsgroups}}.
Words found in the training data are used as the vocabulary. 
Every document is represented as vector of counts over words
present in the vocabulary. 

\subsection{Results}
\label{sec-3-3}
The results are reported on two class splits.
A smoothing value of .01 is used for Naive Bayes implementations
using Binomial and Multinomial. 
Any of the implementation details missing from this document can be
found here\footnotemark[1]{}

\subsubsection{Experiment 1}
\label{sec-3-3-1}

The document classes for experiment 1 are restricted to the following four classes: 
\begin{itemize}
\item alt.atheism
\item talk.religion.misc
\item comp.graphics
\item sci.space
\end{itemize}

As can be seen, the atheism and religion class are confusing and so
are graphics and sci.space. 

The performance of all the models on this data split can be found
in table \ref{tab:4-class}.


\begin{table}[htb]
\caption{\label{tab:4-class}Accuracy on the four-class dataset}
\centering
\begin{tabular}{lr}
\hline
Classifier & Accuracy\\
\hline
RidgeClassifier & 0.631\\
Perceptron & 0.729\\
PassiveAggressiveClassifier & 0.705\\
KNeighborsClassifier & 0.339\\
RandomForestClassifier & 0.727\\
LinearSVC & 0.693\\
SGDClassifier & 0.732\\
LinearSVC & 0.712\\
SGDClassifier & 0.690\\
SGDClassifier & 0.725\\
NearestCentroid & 0.613\\
MultinomialNB & \textbf{0.777}\\
BernoulliNB & 0.722\\
PolyaNB & \textbf{0.783}\\
Pipeline & 0.698\\
\hline
\end{tabular}
\end{table}

\subsubsection{Experiment 2}
\label{sec-3-3-2}
In this experiment, all the categories are used both during training and testing.

\begin{table}[htb]
\caption{\label{tab:20-class}Accuracy on the 20 class dataset}
\centering
\begin{tabular}{lr}
\hline
Model & Accuracy\\
\hline
RidgeClassifier & 0.247\\
Perceptron & 0.567\\
PassiveAggressiveClassifier & 0.568\\
KNeighborsClassifier & 0.194\\
RandomForestClassifier & 0.615\\
LinearSVC & 0.612\\
SGDClassifier & 0.562\\
LinearSVC & 0.618\\
SGDClassifier & 0.535\\
SGDClassifier & 0.563\\
NearestCentroid & 0.315\\
MultinomialNB & \textbf{0.653}\\
BernoulliNB & 0.567\\
PolyaNB & \textbf{0.654}\\
Pipeline & 0.589\\
\hline
\end{tabular}
\end{table}

As can be seen in \ref{tab:4-class} and \ref{tab:20-class}, PolyaNB does the best
and consistently out-performs the MultinomialNB model.

\subsection{When does burstiness modelling help?}
\label{sec-3-4}

\begin{table}[htb]
\caption{\label{tab:p_m}The categories on which PolyaNB did better than MultinomialNB}
\centering
\begin{tabular}{lrr}
\hline
Class & PolyaNB & MultinomialNB\\
\hline
comp.graphics & 0.59 & 0.50\\
comp.sys.mac.hardware & 0.64 & 0.59\\
rec.autos & 0.73 & 0.69\\
rec.motorcycles & 0.74 & 0.72\\
sci.crypt & 0.75 & 0.73\\
talk.politics.mideast & \textbf{0.82} & 0.59\\
\hline
\end{tabular}
\end{table}

\begin{table}[htb]
\caption{\label{tab:m_p}Categories on which MultinomialNB did better than PolyaNB}
\centering
\begin{tabular}{lrr}
\hline
Class & PolyaNB & MultinomialNB\\
\hline
comp.sys.ibm.pc.hardware & 0.48 & 0.52\\
comp.windows.x & 0.73 & 0.75\\
misc.forsale & 0.79 & 0.82\\
soc.religion.christian & 0.61 & 0.78\\
talk.politics.guns & 0.60 & 0.62\\
\hline
\end{tabular}
\end{table}

Table \ref{tab:p_m} shows the categories on which Polya model did better than Multinomial and \ref{tab:m_p}
shows categories for which Multinomial did better than Polya.
Polya model does better on categories that are likely to contain several rare words (\emph{graphics,
hardware, mideast}) and falls short on categories dominated by common words (christian).

Given that the newsgroup dataset is a mix of several such categories, it is not surprising that
PolyaNB does only a little better than MultinomialNB.
\section{Discussion and Conclusion}
\label{sec-4}
Generative Models (even with the Naive Bayes assumption) perform
better than the discriminative classifiers.

Burstiness modelling does not drastically improve performance in all
the cases. In the case of 20 classes, burstiness modelling only
improved accuracy for some of the classes, giving an overall improvement of .1\% on accuracy.

To answer the question: \emph{What's the biggest sin of Naive Bayes? Not
modelling the burstiness or word correlations?}
Given the fact that burstiness modelling did not improve performance
much, it is not unreasonable to conclude that the Naive Bayes lacks in
modelling the word correlations more than in modelling the word distributions.
% Emacs 24.5.1 (Org mode 8.2.10)
\end{document}

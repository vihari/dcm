import sys
import scipy as sp
import scipy.stats as stats
from scipy.special import (psi, polygamma, gammaln, digamma)
from numpy import (array, asanyarray, ones, arange, log, diag, vstack, exp,
        asarray, ndarray, zeros, isscalar)
from numpy.linalg import norm
import numpy as np

class Polya:

    def __init__(self, params=None):
        if params is not None:
            self.params = np.asarray(params)
        
    def logprob(self, data):
        return Polya._logprob(self.params, data)

    @staticmethod
    def _logprob(a, data):
        """
        POLYA_LOGPROB(a,data) returns a vector containing the log-probability of 
        each histogram in DATA, under the Polya distribution with parameter A.
        DATA is a matrix of histograms with histograms on the rows
        """
        
        if np.any(a < 0):
            return -np.Inf
        
        s = np.sum(a)
        nrows, ncols = np.shape(data)
        p = np.zeros([nrows]);

        if False:
            sdata = np.sum(data, axis=1)
            for k in range(ncols):
                dk = data[:, k]
                _ = Polya.sp_digamma(a[k], dk);
                p = p + _
            p = p - Polya.sp_digamma(s, sdata);
        else:
            alpha = a
            sdata = np.sum(data, axis=1)
            for i in range(nrows):
                _1, _2 = np.sum(gammaln(data[i] + alpha)), gammaln(sdata[i] + s)
                p[i] = _1-_2
            _3, _4 = gammaln(s), np.sum(gammaln(alpha))
            p = p + _3-_4
            """
            for i in range(nrows):
                p[i] = sum(data[i]*np.log(data[i]-1 + a));
                p[i] = p[i] - sdata[i]*np.log(sdata[i]-1 + s);
            """

        return p

    @staticmethod
    def _init(D):
        _i = [1.]*np.shape(D)[1]
        return _i

    def sample(self, n):
        """ 
        sample(n) returns a matrix of histograms.
        n is the array of inetegers, where n[i] denotes the number of rows in the output
        for ex: n = [10, 5] generate two polya samples one with 10 and other with 5 experiments
        """
        p = np.random.dirichlet(self.params, size=len(n))
        r = np.zeros([len(n), len(self.params)])
        for i in range(len(n)):
            r[i] = np.random.multinomial(n[i], p[i])
        return r

    @staticmethod
    def sp_digamma(x, n):
        """ returns digamma(x+n)-digamma(x), 
        with special attention to the case n==0.
        """
   
        i = np.where(n > 0)
        y = np.zeros(np.shape(n))
        if not hasattr(x, "__len__"):
            y[i] = digamma(x+n[i]) - digamma(x)
        else:
            y[i] = digamma(x[i]+n[i]) - digamma(x[i])
        return y

    def fit(self, data):
        """ 
        Simple fixed-point iteration described in
        "Estimating a Dirichlet distribution" by T. Minka. 
        """

        show_progress = True
        data = np.ndarray.astype(data, np.float64)
        sdata = np.sum(data, axis=1)
        
        a = self._init(data)
        # fixed-point iteration
        N, K = np.shape(data)
        for iter in range(1000):
            old_a = a
            sa = np.sum(a)
            g = np.sum(Polya.sp_digamma(np.repeat(np.reshape(a, [1, -1]), N, axis=0), data), axis=0)
            h = np.sum(Polya.sp_digamma(sa, sdata))
            a = a * g / h
            import sys
            if show_progress:
                if iter%1==0:
                    sys.stdout.write('\rIter: %03d/1000' % iter)
                    sys.stdout.flush()
            #        print ("Iteration %d: Log Prob %f" % (iter, np.sum(Polya._logprob(a, data))))
            _v = abs(a - old_a)
            if np.max(_v) < 1e-3:
                break

        sys.stdout.write('\n')
        print ("Converged in %d steps" % iter)
        self.params = a+.01*np.min(a[np.where(a>0)])

    @staticmethod
    def test():
        K = 1000
        _p = 1./K
        dcm = Polya([10]*K)
        data = dcm.sample([1000]*1000)
        dcm = Polya.initialize(data)
        dcm.fit(data)

if __name__=='__main__':
    Polya.test()

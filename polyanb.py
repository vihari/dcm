from polya import Polya
import numpy as np

class PolyaNB:
    def __init__(self):
        pass

    def initialize(self, x, y):
        self.num_classes = len(np.unique(y))
        self.num_features = np.shape(x)[1]
        self.dists = [Polya() for i in range(self.num_classes)]
        self.priors = [0 for i in range(self.num_classes)]

    def fit(self, X_train, Y_train):
        self.initialize(X_train, Y_train)

        if not isinstance(X_train, np.ndarray):
            X_train = X_train.todense().A
        # we fit a polya for each class
        N = np.shape(X_train)[0]
        for c in range(self.num_classes):
            idx = np.where(Y_train==c)
            x = X_train[idx]
            n_y = np.shape(x)[0]
            x = np.reshape(x, [n_y, -1])

            self.dists[c].fit(x)
            self.priors[c] = float(n_y)/N
    
    def predict(self, X_test):
        if not isinstance(X_test, np.ndarray):
            X_test = X_test.todense().A

        ps = np.asarray([[np.log(self.priors[c]) + self.dists[c].logprob(np.reshape(row, [1, -1])) for row in X_test] for c in range(self.num_classes)])
        ps = np.reshape(ps, [self.num_classes, -1])
        print (ps)
        return np.argmax(ps, axis=0)
            
    @staticmethod
    def test():
        nb = PolyaNB()
        p1 = Polya([0.1]*5+[0.5])
        p2 = Polya([0.5] + [0.1]*5)
        x1 = p1.sample([1000]*10000)
        x2 = p2.sample([1000]*10000)
        x = np.concatenate([x1, x2], axis=0)
        y = np.concatenate([[0]*10000, [1]*10000], axis=0)
        nb.fit(x, y)
        _y = nb.predict(p1.sample([1000]*100))
        print (_y)
        _e = (100-sum(_y))/100.
        print ("Accuracy: %.2f" % _e)
        _y = nb.predict(p2.sample([1000]*100))
        _e = sum(_y)/100.
        print ("Accuracy: %.2f" % _e)

if __name__=='__main__':
    PolyaNB.test()

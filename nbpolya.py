from . import Polya

class PolyaNB:
    def __init__(self, num_classes):
        self.num_classes = num_classes
        self.dists = [Polya() for i in range(num_classes)]

    def fit(self, X_train, Y_train):
        # we fit a polya for each class
        for c in range(num_classes):
            x = X_train[np.argwhere(Y_train==c)]
            dists[c].fit(X)
    
    def predict(self, X_test):
        ps = np.asarray([dists[c].logprob(X_test) for c in range(num_classes)])
        return np.argmax(ps)
            
